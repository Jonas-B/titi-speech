module.exports = class Container {
  constructor() {
    this.dependencies = {}
  }

  set(name, value, options = {}) {
    this.dependencies[name] = {
      value: value,
      options: options
    };
  }

  get(name) {
    return this.dependencies[name].value;
  }

  service(name) {
    return this.get('service.' + name);
  }

  bootstrap(force = false) {
    let promises = [];
    for (let name in this.dependencies) {
      if (
        this.dependencies[name].options.bootstrap == null || 
        (
          this.dependencies[name].options.bootstrap != null && 
          this.dependencies[name].options.bootstrap != force
        )
      ){
        if (this.dependencies[name].value instanceof Function) {
          this.dependencies[name].value = this.dependencies[name].value(this.dependencies);
        }
        if (this.dependencies[name].value instanceof Promise) {
          promises.push(this.dependencies[name].value)
        }
        if (this.dependencies[name].options.method != null) {
          promises.push(this.get(name)[this.dependencies[name].options.method]())
        }
      }
    }
    return Promise.all(promises);
  }
}
