require('dotenv').config()
require('./helper')

global.container = require('./container');

module.exports = container.bootstrap()