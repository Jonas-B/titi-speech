const winston = require('winston');
const { format } = require('logform');
const moment = require('moment');

var Container = require('./Services/Container');

var container = new Container;

container.set('env', (() => {
  let env_vars = {};
  for (let env_var in process.env) {
    env_vars[env_var] = process.env[env_var];
    global[env_var] = process.env[env_var];
  }
  return env_vars;
})())

container.set('service.logger', () => {
  let customTimestamps = format((info, opts) => {
    info.timestamp = moment().format('HH:mm:ss');

    return info;
  })

  let logger = winston.createLogger({
    level: process.env.LOG_LEVEL,format: format.combine(
      customTimestamps(),
      format.printf(info => `[${info.timestamp}] ${info.level}: ${info.message}`)
    ),
    transports: [
      new winston.transports.File({ filename: 'log/error.log', level: 'error' }),
      new winston.transports.File({ filename: 'log/combined.log' })
    ]
  });

  if (process.env.NODE_ENV !== 'production') {
    logger.add(new winston.transports.Console({
      format: format.combine(
        format.colorize(),
        customTimestamps(),
        format.printf(info => `[${info.timestamp}] ${info.level}: ${info.message}`)
      )
    }));
  }

  return logger;
})

module.exports = container;