'use strict'

let bootstrap = require('../../src/bootstrap');
let Room = require('../../src/Business/Model/Room');
let Device = require('../../src/Business/Model/Device');
let Action = require('../../src/Business/Model/Action');

module.exports.up = function (next) {
  bootstrap.then(() => {
    Promise.all([
      Room.create({ name: 'Kitchen' }),
      Room.create({ name: 'Bedroom' }),
      Room.create({ name: 'Living room' }),
      Room.create({ name: 'Bathroom' }),
      Device.create({ name: 'Lamp' }),
      Device.create({ name: 'Music player' }),
      Action.create({ name: 'On/Off' }),
      Action.create({ name: 'Up/Dowm' }),
    ])
    .then(values => {
      next();
    })
    .catch(err => container.service('logger').error(err))
  })
}

module.exports.down = function (next) {
  next()
}
